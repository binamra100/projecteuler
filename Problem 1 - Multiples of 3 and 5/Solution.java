/**
 *
 * @author Binamra Kandel
 */
public class Solution {

    public static void main(String[] args) {
        int sum = 0, max = 1000;
        
        for(int i = 0; i < max; i++){
            if((i % 3 ==0)||(i % 5 ==0))
                sum = sum + i;
        }
        
        System.out.println(sum);
        
    }
    
}
