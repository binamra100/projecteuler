package com.binamra.EvenFibonacciNumbers;

import java.util.ArrayList;

/**
 *
 * @author Binamra Kandel
 */
public class Solution {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int i = 1, j = 2, nextterm = 0, total = 0;
        do{
            nextterm = i + j;
            i = j;
            j = nextterm;
            
            if(nextterm%2==0)
                total = total + nextterm;
            
        }while(nextterm < 4000000);
        
        System.out.println(total);
    }
    
}
